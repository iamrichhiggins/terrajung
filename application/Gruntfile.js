module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		concat: {   
		    dist: {
		        src: [
		            'webroot/wp-content/themes/terrajung/js/libs/jquery-1.10.2.min.js',
		            'webroot/wp-content/themes/terrajung/js/libs/bootstrap.min.js',
		            'webroot/wp-content/themes/terrajung/js/global.js'
		        ],
		        dest: 'webroot/wp-content/themes/terrajung/js/build/production.js',
		    }
		},
		
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'webroot/wp-content/themes/terrajung/scss',
					src: ['*.scss'],
					dest: 'webroot/wp-content/themes/terrajung',
					ext: '.css'
				}]
			}
		},

		watch: {	
		    scripts: {
		        files: ['webroot/wp-content/themes/terrajung/js/*.js'],
		        tasks: ['concat'],
		        options: {
		            spawn: false
		        },
		    },			
			sass: {
			    files: ['webroot/wp-content/themes/terrajung/scss/*.scss'],
			    tasks: ['sass'],
			    options: {
			        spawn: false,
			    }
			},
			livereload: {
  		      files: ['webroot/wp-content/themes/terrajung/*.css', 'webroot/wp-content/themes/terrajung/*.php', 'webroot/wp-content/themes/terrajung/js/*.js'],
		      options: { 
				  livereload: true 
			  }
		    }
		}

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'sass']);	

};