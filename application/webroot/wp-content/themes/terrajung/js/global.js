var nav = {
	anchors:null,
	init:function(){
		$('.l-fixed').addClass('l-fixed-js');	// add js classes
		$('.section-header-portfolio').addClass('section-header-portfolio-js');
				
		this.anchors = $('.header-nav > li > a');		
		this.anchors.click(function(e){	// animate internal anchors
			e.preventDefault();	
		
			anchor = $(this).data('scroll');		
			offset = $(anchor).offset().top;		
			anchorpos = offset - 220;
		
			if(this.href.match(/resume/))
				anchorpos = offset - 46;
		
			if($(window).width() >= 600)
				anchorpos = offset - 230;		
			else if($(window).width() >= 960)
				anchorpos = offset - 210;
		
			nav.anchors.removeClass('selected');
			$(this).addClass('selected');
			$('html, body').animate({scrollTop:anchorpos}, 300);
		});
	}
}

var header = {
	background:null,
	heading1:null,
	subHeadings:null,
	init:function(){		
		this.background = $('.l-background');
		this.heading1 = $('.header > h1');
		this.subHeadings = $('.sub-header, .contact');
		
		$(window).scroll(function(){	// minimise header for small screens	
		
			if($(window).width() < 569){
				if($(window).scrollTop() > 112){
					header.background.addClass('is-minimised');
					header.heading1.addClass('is-offscreen');
					header.subHeadings.addClass('is-hidden');		
				}
				else{
					header.background.removeClass('is-minimised');
					header.heading1.removeClass('is-offscreen');
					header.subHeadings.removeClass('is-hidden');						
				}			
			}
		});
	}
}


var carousel = {
	control:null,
	init:function(){
		this.carouselControl = $('.carousel-control');
		this.carouselControl.addClass('carousel-control-js');	// add js classes
		this.loadImages();
	},	
	loadImages:function(){
		$('article:lt(3) .item > img').each(function(){
			$(this).attr('src',$(this).data('src'));
		});		
	},
	loadMoreImages:function(){
		portfolio.more.find('.item > img').each(function(){
			$(this).attr('src',$(this).data('src'));
		});
	}
}

var portfolio = {
	more:null,
	last:null,
	init:function(){		
		this.more = $('article:gt(2)');	// hide gt(2)
		this.more.addClass("is-hidden");
		
		this.last = $('article').last();	// append show more link
		this.last.after('</div><div class="l-container"><div class="text"><p class="view-all"><a href="#">&raquo; More projects</a></p></div></div>');
		
		$('.view-all').click(function(e){	// show more projects
			e.preventDefault();
			carousel.loadMoreImages();
			portfolio.more.removeClass("is-hidden");
			$(this).remove();
		});				
	}
}

$(function(){	
	nav.init();
	header.init();
	carousel.init();
	portfolio.init();
});