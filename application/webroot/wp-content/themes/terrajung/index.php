<!doctype html>
<html lang="en">
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<head>
	<meta charset="UTF-8" />
	<title><?php bloginfo();?></title>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" />
	<script async="async" src="<?php echo get_template_directory_uri(); ?>/js/build/production.js"></script>
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48856016-1', 'terrajung.com');
	  ga('send', 'pageview');

	</script>
</head>
<body>
	
	<header>

		<div class="l-fixed">
			<div class="l-background">
				<div class="l-container-overflow">				
					<div class="header">
						<h1>Terra</h1>				
						<nav>
							<ul class="header-nav">
								<li><a data-scroll="#portfolio" href="#portfolio" class="selected">Portfolio</a></li>
								<li><a data-scroll="#resume" href="#resume">Résumé</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		
			<div class="l-container">	
				<div class="l-column-640">
					<h2 class="sub-header">User Experience. Usability. Information architecture. Design. Web. Mobile. Tablet</h2>				
				</div>	
				<div class="l-column-280">
					<ul class="contact">
						<li><a href="mailto:terra@terrajung.com">terra@terrajung.com</a></li>
						<li>647 237 7612</li>
					</ul>
				</div>			
			</div>
		</div>	
		
	</header>	
				
	<section>
		
		<div class="l-container">

<?php 
			$post = get_page_by_title('Portfolio');
			setup_postdata( $post );
?>

			<header>
				<h1 class="section-header section-header-portfolio" id="portfolio"><?php the_title();?></h1>
			</header>
		
			<div class="text">
				<?php the_content();?>
			</div>
			
		</div>
			
<?php
			$page = get_page_by_title('Portfolio');
			$pages = get_pages(array('child_of'=>$page->ID,'sort_column' => 'menu_order'));

			foreach($pages as $index => $page){			
				
				$post = $page;
				setup_postdata( $post );
?>				

			<!-- start portfolio item -->
			
			<article>
				
				<div class="l-container">
			
					<header>					
						<h1 class="article-header"><?php the_title();?></h1>
					</header>
					
					<div class="l-column-600 l-column-right">			
						<div class="carousel slide" data-interval="0" id="Carousel<?php echo $post->ID;?>">							
					        <div class="carousel-inner">
								
					<?php						
							for($i=1;$i<6;$i++){				
								$imageField = 'image'.$i;	// set custom field names					
								$image = get_field($imageField);	// fetch image
								if(!empty($image)){	// test image
					?>
					  			<div class="item <?php echo $i==1 ? 'active' : '';?>"><img data-src="<?php echo $image['url']; ?>" src="<?php echo $i==1 ? $image['url'] : get_template_directory_uri().'/img/blank.png'; ?>" alt="<?php echo $image['title']; ?>"></div>
					<?php						
								}
							}
					?>
						  </div>
					      <a class="left carousel-control" href="#Carousel<?php echo $post->ID;?>" data-slide="prev">&lt;</a>
					      <a class="right carousel-control" href="#Carousel<?php echo $post->ID;?>" data-slide="next">&gt;</a>
						</div>
						
					</div>	
					
					<div class="l-column-280">
						<div class="text">			
							<?php the_content();?>			
						</div>	
					</div>				
					
					<div class="l-column-600 l-column-right">
						
						<div class="l-column-280">	
							<div class="text">	
								<?php the_field('text_column_2');?>
							</div>
						</div>

						<div class="l-column-280 l-column-right">	
							<div class="text">	
								<?php the_field('text_column_3');?>
							</div>
						</div>
					
					</div>
			
				</div>
							
			</article>
			
			<!-- end portfolio item -->
<?php
			}
?>					
		
	</section>
	
	<section>
		
		<div class="l-container">

<?php 
			$post = get_page_by_title('Résumé');
			setup_postdata( $post );
?>

			<header>
				<h1 class="section-header" id="resume">Résumé</h1>
			</header>
			
			<div class="l-column-600">				
				
				<div class="text">
<?php						
				$resume = get_field('resume');	// fetch file
				if(!empty($resume)){	// test file						
					$filesize = filesize( get_attached_file( $resume['id'] ) );
					$filesize = size_format($filesize);
?>

					<p><span class="icon-file-pdf"></span> <a href="<?php echo $resume['url']; ?>">Resume - PDF version (<?php echo $filesize;?>)</a></p>
<?php						
				}
				
				the_content();
?>
				</div>
			</div>	
						
			<div class="l-column-280 l-column-right">	
								
<?php						
				$image = get_field('logos');	// fetch image
				if(!empty($image)){	// test image
?>
				<p class="logos"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" /></p>
<?php						
				}
?>
			</div>
			
		</div>
		
	</section>
		
<?php
	    if(isset($_SERVER['SERVER_NAME'])) {
	        if(preg_match('/local|192/',$_SERVER['SERVER_NAME'])){
?>	
	<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<?php
		    }
		}
?>		

	<?php wp_footer(); ?>
		
</body>
</html>