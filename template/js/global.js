$(function(){
	
	// animate scroll links
	$('.header-nav > li > a').click(function(e){		
		e.preventDefault();
		anchor = $(this).data('scroll');		
		
		anchorpos = $(anchor).offset().top;
		
		alert($(window).width());
		
		if($(window).width() >= 960)
			anchorpos = $(anchor).offset().top - 210;
		
		$('.header-nav > li > a').removeClass('selected');
		$(this).addClass('selected');
		$('html, body').animate({scrollTop:anchorpos}, 300);
	});
	
	// init portfolio carousels
	$('.carousel').carousel('pause');
	
});