<!doctype html>
<html lang="en">
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<head>
	<meta charset="UTF-8" />
	<title>Terra Jung  - User Experience Architect in Toronto, Canada</title>
	<link rel="stylesheet" href="/css/style.css" />
	<script type="text/javascript">
	    (function() {
	        var path = '//easy.myfonts.net/v1/js?sid=215489(font-family=ITC+Avant+Garde+Gothic+Std+DemiBold)&sid=215493(font-family=ITC+Avant+Garde+Gothic+Std+Medium)&sid=215483(font-family=ITC+Avant+Garde+Gothic+Std+Book)&sid=215491(font-family=ITC+Avant+Garde+Gothic+Std+ExtraLight)&key=QKV8tIxZsW',
	            protocol = ('https:' == document.location.protocol ? 'https:' : 'http:'),
	            trial = document.createElement('script');
	        trial.type = 'text/javascript';
	        trial.async = true;
	        trial.src = protocol + path;
	        var head = document.getElementsByTagName("head")[0];
	        head.appendChild(trial);
	    })();
	</script>
	<script async="async" src="/js/build/production.js"></script>
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
</head>
<body>
	
	<header>

		<div class="l-fixed">
			<div class="l-background">
				<div class="l-container-overflow">				
					<div class="header">
						<h1>Terra</h1>				
						<nav>
							<ul class="header-nav">
								<li><a data-scroll="#portfolio" href="#portfolio" class="selected">Portfolio</a></li>
								<li><a data-scroll="#resume" href="#resume">Résumé</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		
			<div class="l-container">	
				<div class="l-column-640">
					<h2 class="sub-header">User Experience. Usability. Information architecture. Design. Web. Mobile. Tablet</h2>				
				</div>	
				<div class="l-column-280">
					<ul class="contact">
						<li><a href="mailto:terra@terrajung.com">terra@terrajung.com</a></li>
						<li>647 237 7612</li>
					</ul>
				</div>			
			</div>
		</div>	
		
	</header>	
				
	<section>
		
		<div class="l-container">
		
			<heading>
				<h1 class="section-header section-header-portfolio" id="portfolio">Portfolio</h1>
			</heading>
		
			<div class="text">
		
				<p>The following are some of the websites and applications that I have produced during my 16 year career. My role has varied from user interface designer to information architect to usability specialist, depending on the project. However, regardless of the role I played, all designs were created with keeping in mind the end-user as well as the needs of the client.</p>

				<p><em>Please note that some of the sites have been redesigned subsequent to my working on them.</em></p>
	
			</div>
			
		</div>
			
<?php
			for($i=0;$i<3;$i++){
?>				
			<!-- start portfolio item -->
			
			<article>
				
				<div class="l-container-overflow">
			
					<heading>					
						<h1 class="article-header">Jeff Goodman Studio</h1>
					</heading>

					<div class="l-column-600 l-column-right">			
						<div class="carousel slide" id="Carousel<?php echo $i;?>">							
					        <div class="carousel-inner">
					          <div class="item active"><img src="/img/content/jgs-01.jpg" width="558" height="398" alt="Jgs 01"></div>
					          <div class="item"><img src="/img/content/jgs-01.jpg" width="558" height="398" alt="Jgs 01"></div>
					          <div class="item"><img src="/img/content/jgs-01.jpg" width="558" height="398" alt="Jgs 01"></div>
						  </div>
					      <a class="left carousel-control" href="#Carousel<?php echo $i;?>" data-slide="prev"><</span></a>
					      <a class="right carousel-control" href="#Carousel<?php echo $i;?>" data-slide="next">></a>
						</div>
					</div>					
			
					<div class="l-column-280">						
						<div class="text">
				
							<p><strong>Role: </strong>Web Designer, Information Architect<br />
								<strong>Company: </strong>Jeff Goodman<br />
								<strong>Website: </strong><a href="http://www.jeffgoodmanstudio.com">www.jeffgoodmanstudio.com</a>
							</p>

							<p><strong>Description:</strong><br />A new website was designed for Canadian glass artist Jeff Goodman, that showcases the glass vessels and architectural glass designs. The design is clean and contemporary and expresses the artistic sensibilities of the pieces. The strategic goal for the website is to make the vessels immediately accessible to art collectors and galleries regardless of what city they may be in.</p>

							<p>A responsive approach to the design was taken allowing for better desktop, tablet and mobile experiences.</p>
				
						</div>					
					</div>
					
				</div>
							
			</article>
			
			<!-- end portfolio item -->
<?php
			}
?>					
		</div>
		
	</section>
	
	<section>
		
		<div class="l-container">
		
			<heading>
				<h1 class="section-header" id="resume">Résumé</h1>
			</heading>
			
			<div class="l-column-600">				
				<div class="text">
							
					<h2>Profile and Core Competencies</h2>
		
					<ul>				
						<li>An award winning, user experience designer with over 16 years experience, focusing on web interface design and usability.</li>
						<li>Worked in both private and public sectors for companies such as Oxfam UK, Nike Foundation, Greenpeace UK, Sony PlayStation, Dr. Martens, Umbro, Bell Canada, AOL, Symantec, YHA, and Manchester Art Gallery.</li>
						<li>Expertise in defining, designing, developing and supporting commercial websites and web applications from initial concept through to implementation.</li>
						<li>Ability to successfully marry usability, functionality and brand equity in all aspects of work.</li>
						<li>A highly organized individual with excellent problem solving and analytical skills.</li>
						<li>A team player, who can effectively motivate and communicate with all members within an organisation.</li>
						<li>Ability to work closely with clients and nurture the relationship and successfully manage their expectations.</li>
					</ul>
		
					<h2>Technical Skills</h2>
		
					<h3>Usability/User Experience</h3>
					<p>An expert in usability issues, methodologies, and best practices. Experience in designing and facilitating various user research activities such as prototype testing, card sorting, tree testing, contextual inquiry, usability audits, usability testing, A/B testing, questionnaires, and surveys.</p>

					<h3>Information Architecture</h3>
					<p>Extensive experience in defining the architecture of complex websites and web applications, developing use cases, site maps, wire-frames, workflow diagrams, and functional/UI specifications.</p>

					<h3>Interface Design</h3>
					<p>Strong visual background with experience in graphic design, page layout, image optimisation, and brand equity. Experience in designing innovative responsive interfaces (desktop, tablet and mobile) with an emphasis on user friendly design.</p>

					<h3>Technology</h3>
					<p>A good understanding of web technologies with experience in database driven web applications and content management systems. Familiar with cross browser and platform compatibility, basic knowledge of HTML and CSS.</p>

					<h3>Accessibility</h3>
					<p>A good understanding of W3C accessibility guidelines and designing websites that meet A, AA and AAA standards.</p>

					<h3>Software</h3>
					<p>Extensive experience on both PC and Mac platforms. Proficient at OmniGraffle, Visio, Axure Pro, Photoshop, Illustrator, Flash, Fireworks, Dreamweaver, BBEdit, QuarkXpress, Acrobat Pro, and Microsoft Office.</p>
				
				</div>
			</div>		
			
			<div class="l-column-280">	
				Resume - PDF version
			</div>
			
		</div>
		
	</section>
	
<?php
	    if(isset($_SERVER['SERVER_NAME'])) {
	        if(preg_match('/local|192/',$_SERVER['SERVER_NAME'])){
?>	
	<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<?php
		    }
		}
?>		

</body>
</html>